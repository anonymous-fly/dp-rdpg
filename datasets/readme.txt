Downloaded From: http://proj.ise.bgu.ac.il/sns

Using our organization social network crawler, we collected data from six companies on three different scales: Small (S), Medium (M), and Large (L) scale companies currently employing 500 to 2,000, 4,000 to 20,000, and more than 50,000 employees, respectively. 
The data organizations' datasets were collected form the organizations' employees Facebook pages. 
More details on each organization can be found in our paper "Organization Mining Using Online Social Networks".
Please Cite:

Organization Mining Using Online Social Networks , Michael Fire, Rami Puzis, and Yuval Elovici, 2012.
Bibtex:
@article{

title={Organization Mining Using Online Social Networks},
author={Michael Fire, Rami Puzis, Yuval Elovici},
year={2012}
}
